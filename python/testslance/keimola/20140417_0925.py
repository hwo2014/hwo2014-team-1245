import sys
import json
import socket
import sys
import time
import math

PIECE_TURN = 1
PIECE_STRAIGHT = 2
PIECE_SWITCH = 4
D2R = math.pi / 180.0
class Pieces:
	def __init__ (self, data):
		pieces = data["race"]["track"]["pieces"]
		self.turnToType = {}
		self.turnToLength = {}
		self.turnToRadius = {}
		for i, aPiece in enumerate ( pieces ):
			if "angle" in aPiece and "switch" in aPiece:
				self.turnToType[i] = PIECE_TURN | PIECE_SWITCH
			elif "angle" in aPiece:
				self.turnToType[i] = PIECE_TURN
			elif "switch" in aPiece:
				self.turnToType[i] = PIECE_SWITCH
				print 'Piece %d is a switch' % ( i )
			else:
				self.turnToType[i] = PIECE_STRAIGHT
			if "length" in aPiece:
				self.turnToLength[i] = aPiece["length"]
				print 'Piece %d len %d' % ( i, self.turnToLength[i] )
			elif "angle" in aPiece:
				self.turnToLength[i] = abs(aPiece["angle"]) * D2R * aPiece["radius"]
				self.turnToRadius[i] = aPiece["radius"]
				print 'Piece %d len %d radius %.2f' % ( i, self.turnToLength[i], self.turnToRadius[i] )
		self.numTurns = len ( self.turnToType.keys() )

	def getPieceLength ( self, turnNumber ):
		return self.turnToLength[turnNumber]

	def getPieceType ( self, turnNumber ):
		return self.turnToType[turnNumber]

	def getNextTurn ( self, turn ):
		return ( turn + 1 ) % self.numTurns

	def getNextPieceType ( self, turn ):
		return self.turnToType[( turn + 1 ) % self.numTurns]

	def getRadius ( self, turn ):
		if ( PIECE_TURN & self.turnToType[turn] ):
			return self.turnToRadius[turn]
		else:
			return 1e20

class Logger:
	def __init__ (self, path):
		self.fp = open(path, 'wb')
	def client ( self, msg ):
		self._log ("client", msg)
	def server ( self, msg ):
		self._log ("server", msg)
	def _log(self, who, msg):
		out = "%s,%s,%s" % ( time.time(), who, msg )
		self.fp.write(out)
		self.fp.flush()
	def close ( self ):
		self.fp.close()

class NoobBot(object):

    def __init__(self, socket, name, key):
	self.log = Logger ("./data.txt")
        self.socket = socket
        self.name = name
        self.key = key
	self.trackPieces = None
	self.turnVelocity = 0.5
	self.lastDistance = 0
	self.lastAngle = 0
	self.lastTick = 0
	self.lastSlipSign = 1
	self.mode = 'a'
	self.tick = 0
	self.muSum = 0.0
	self.muCnt = 0
	self.muAvg = 0.0

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")
	self.log.client ( msg + "\n" )

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_game_init ( self, data ):
    	self.trackPieces = Pieces ( data )

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
	for q in data:
		if not q["id"]["name"] == "BananaStand":
			continue
		us = q
		break

	turn = us["piecePosition"]["pieceIndex"]
	currDist = us["piecePosition"]["inPieceDistance"]
	angle = us["angle"]
	pieceLength = self.trackPieces.getPieceLength(turn)
	turnType = self.trackPieces.getPieceType(turn) 
	nextTurnType = self.trackPieces.getNextPieceType(turn)
	radius = self.trackPieces.getRadius (turn)
	mu = None
	if ( currDist < self.lastDistance ):
		speed = currDist
	else:
		speed = currDist - self.lastDistance
		if ( radius < 1e10 and ( turn & PIECE_TURN ) ):
			mu = ( speed * speed ) / radius
			self.muSum += mu
			self.muCnt += 1
			self.muAvg = self.muSum / self.muCnt
			
		
	self.lastDistance = currDist
	slipRate = angle - self.lastAngle
	slipSign = [-1,1][slipRate>0]
	absSlipRate = abs(slipRate)
	self.lastAngle = angle
	self.zeroCrossing = not ( slipSign == self.lastSlipSign )
	absAngle = abs(angle)
	self.lastSlipSign = slipSign

	#
	#self.throttle(0.5) # --> mu = 0.25
	# muMax muAvg(obs)
	#  0.35	  0.3355
	#  0.40   0.3728
	#  0.45   0.3898
	#  0.465  0.3909
	#  0.47   0.3927
	#  0.48   0.3954
	#  0.485  0.3970 * fastest
	#  0.4875 0.4007 
	#  0.48875 0.4007
	#  0.49   0.3896
	muMax = 0.485
	wantSpeed =  ( radius * muMax ) ** 0.5
	percComplete = currDist / pieceLength


	# DEBUG
	#if turn in [3,4,5,14,15,16] or 0 == self.turnVelocity or self.mode == 'b'or absAngle > 9.0 or ( 0 == (self.tick + 1) % 13 ):
	if 0 == self.turnVelocity or self.mode == 'b'or absAngle > 9.0 or ( 0 == (self.tick + 1) % 13 ):
		fmt = 'Tick %05d Piece %2d/%d PC %.2f Ang %6.2f Throttle %.2f SR %8.3f Spd %6.2f Want %6.2f M %s SS %2d mu %s/%f\r' % ( self.tick, turn, turnType, percComplete, angle, self.turnVelocity, slipRate, speed, wantSpeed, self.mode, slipSign, mu, self.muAvg )
		sys.stdout.write(fmt)
		#if turn in [3,4,5,14,15,16] or absAngle > 9.0 or self.mode == 'b':
		if absAngle > 9.0 or self.mode == 'b':
			sys.stdout.write("\n")
		sys.stdout.flush()
	

	# TODO: cross over at pieces 13 and 25?
	# TODO: Should really be distance based on breaking performance (might be N pieces ahead)
	if ( nextTurnType & PIECE_TURN ):
		#if ( percComplete >= 0.4 and speed > wantSpeed):
		nextTurnRadius = self.trackPieces.getRadius (self.trackPieces.getNextTurn(turn))
		nextTurnSpeed = ( nextTurnRadius * muMax ) ** 0.5
		if ( speed > nextTurnSpeed ) or (absSlipRate>5.0) or ( absAngle >15 and absSlipRate > 2.0  ):
			self.turnVelocity = 0.0
			self.throttle(self.turnVelocity)
			return

	if PIECE_TURN & turnType:
		#print 'want %f now %f' % ( wantSpeed, speed )
		if ( wantSpeed < speed or absAngle > 40.0):
			self.turnVelocity = 0.0
		elif ( wantSpeed > speed ):
			if absSlipRate < 1.4:
				self.turnVelocity = 1.0
			#else:
			#	self.turnVelocity = 0.5
			if ( self.turnVelocity < 0.5 ):
				self.turnVelocity = 0.5
			else:
				self.turnVelocity *= 1.1
				if self.turnVelocity > 1.0:
					self.turnVelocity = 1.0
	else:
		self.turnVelocity = 1.0
	self.throttle ( self.turnVelocity )
	return

	# in a turn
	if PIECE_TURN & turnType:
		if absSlipRate < 1.4 and (not self.mode == 'b') and self.zeroCrossing:
			self.mode='a'
			#self.turnVelocity = 1.0			
			t = self.turnVelocity * 1.1
			if ( t > 1.0 ):
				t = 1.0
			self.turnVelocity = t
		elif absSlipRate > 3.0 or absAngle > 10.0:
			self.turnVelocity = 0.0
			self.mode = 'b'
		else:
			self.mode = 'a'
			if self.turnVelocity < 0.5:
				self.turnVelocity = 0.5
			else:
				t = self.turnVelocity * 1.1
				if ( t > 1):
					t = 1
				self.turnVelocity = t
		self.throttle(self.turnVelocity)

	# not a turn
	elif PIECE_STRAIGHT & turnType or PIECE_SWITCH & turnType:
		self.mode = 'a'
		if ( nextTurnType & PIECE_TURN ):
			percComplete = currDist / pieceLength
			if ( percComplete >= 0.5 ):
				self.turnVelocity = 0.0
				self.mode = 'b'	
			else:
				self.turnVelocity = 1.0
		else:
			self.turnVelocity = 1.0
		self.throttle ( self.turnVelocity )

    def on_crash(self, data):
        print("\n%s crashed" % data["name"])
        self.ping()

    def on_game_end(self, data):
        print("\nRace ended")
        self.ping()

    def on_error(self, data):
        print("\nError: {0}".format(data))
        self.ping()
	
    def on_spawn (self, data):
    	print "\n%s spawned" % ( data["name"] )
	self.turnVelocity = 1.0
	self.mode = 'a'
	self.throttle(self.turnVelocity)
    
    def on_lap ( self, data ):
    	print "\nLap %d Name %s Time %d" % ( data["lapTime"]["lap"], data["car"]["name"], data["lapTime"]["millis"] )
	self.ping()

    def msg_loop(self):
        msg_map = {
	    'gameInit' : self.on_game_init,
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
	    "spawn": self.on_spawn,
	    'lapFinished' : self.on_lap
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
	    if msg.has_key("gameTick"):
		    self.lastTick = self.tick
		    self.tick = msg["gameTick"]
	    self.log.server ( line )
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
		print("\nGot {0}".format(msg_type))
		self.ping()
            line = socket_file.readline()
	self.log.close()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
	try:
        	bot.run()
	except:
		print 'EXCEPTION:', sys.exc_info()[:2]
		raise
