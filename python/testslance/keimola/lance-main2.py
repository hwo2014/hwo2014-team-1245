import sys
import json
import socket
import sys
import time
import math

PIECE_TURN = 1
PIECE_STRAIGHT = 2
PIECE_SWITCH = 4
D2R = math.pi / 180.0
class Pieces:
	def __init__ (self, data):
		pieces = data["race"]["track"]["pieces"]
		self.turnToType = {}
		self.turnToLength = {}
		for i, aPiece in enumerate ( pieces ):
			if "angle" in aPiece and "switch" in aPiece:
				self.turnToType[i] = PIECE_TURN | PIECE_SWITCH
			elif "angle" in aPiece:
				self.turnToType[i] = PIECE_TURN
			elif "switch" in aPiece:
				self.turnToType[i] = PIECE_SWITCH
			else:
				self.turnToType[i] = PIECE_STRAIGHT
			if "length" in aPiece:
				self.turnToLength[i] = aPiece["length"]
			elif "angle" in aPiece:
				self.turnToLength[i] = aPiece["angle"] * D2R * aPiece["radius"]
		self.numTurns = len ( self.turnToType.keys() )

	def getPieceLength ( self, turnNumber ):
		return self.turnToLength[turnNumber]

	def getPieceType ( self, turnNumber ):
		return self.turnToType[turnNumber]

	def getNextPieceType ( self, turn ):
		return self.turnToType[( turn + 1 ) % self.numTurns]

class Logger:
	def __init__ (self, path):
		self.fp = open(path, 'wb')
	def client ( self, msg ):
		self._log ("client", msg)
	def server ( self, msg ):
		self._log ("server", msg)
	def _log(self, who, msg):
		out = "%s,%s,%s" % ( time.time(), who, msg )
		self.fp.write(out)
		self.fp.flush()
	def close ( self ):
		self.fp.close()

class NoobBot(object):

    def __init__(self, socket, name, key):
	self.log = Logger ("./data.txt")
        self.socket = socket
        self.name = name
        self.key = key
	self.trackPieces = None
	self.turnVelocity = 0.5
	self.lastAngle = 0
	self.mode = 'a'

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")
	self.log.client ( msg + "\n" )

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_game_init ( self, data ):
    	self.trackPieces = Pieces ( data )

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
	for q in data:
		if not q["id"]["name"] == "BananaStand":
			continue
		us = q
		break

	turn = us["piecePosition"]["pieceIndex"]
	currDist = us["piecePosition"]["inPieceDistance"]
	angle = us["angle"]
	pieceLength = self.trackPieces.getPieceLength(turn)
	turnType = self.trackPieces.getPieceType(turn) 
	nextTurnType = self.trackPieces.getNextPieceType(turn)

	if PIECE_TURN & turnType:
		slipRate = abs(angle)-abs(self.lastAngle)
		if slipRate < 1.4:
			self.mode='a'
		if abs(angle) > 10.0 or slipRate > 1 or self.mode=='b':
			self.turnVelocity = 0.0
		else:
			if slipRate < 0:
				self.turnVelocity = 1.0
			elif self.turnVelocity < 0.5:
				self.turnVelocity = 0.5
			else:
				t = self.turnVelocity * 1.15
				if t > 1.0:
					t = 1.0
				self.turnVelocity = t
		self.lastAngle = angle
		print 'Turn %d a %6.2f v %.2f sr %f' % ( turn, angle, self.turnVelocity, slipRate )
		self.throttle(self.turnVelocity)

	elif PIECE_STRAIGHT & turnType or PIECE_SWITCH & turnType:
		if ( nextTurnType & PIECE_TURN ):
			percComplete = currDist / pieceLength
			print 'Straight turn coming up pc %5.1f' % ( percComplete )
			if ( percComplete >= 0.5 ):
				print 'Backing off'
				self.throttle (0.0)
			else:
				self.throttle(1.0)
		else:
			self.throttle (1.0)

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
	    'gameInit' : self.on_game_init,
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
	    self.log.server ( line )
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()
	self.log.close()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
	try:
        	bot.run()
	except:
		print 'EXCEPTION:', sys.exc_info()[:2]
