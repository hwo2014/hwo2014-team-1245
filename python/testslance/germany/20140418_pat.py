import sys
import json
import socket
import sys
import time
import math

PIECE_TURN = 1
PIECE_STRAIGHT = 2
PIECE_SWITCH = 4
D2R = math.pi / 180.0

class Lanes(object):
	allLanes = []

	def addLane(self, laneIndex, distFromCenter):
		self.allLanes.insert(laneIndex, distFromCenter)

	def getDistFromCenter(self, laneIndex):
		return self.allLanes[laneIndex]

	def printAllLanes(self):
		for index, lane in enumerate(self.allLanes):
			print 'Lane %d of distFromCenter %f' % (index, lane)

	def calculateAllDistances(self, piece):
		retVal = []
		for index, distFromCenter in enumerate(self.allLanes):
			pLength = piece.getPieceLength(distFromCenter)
			#print 'Calculating piece index %d at len %d' % ( index, pLength )
			retVal.insert(index,pLength)
		return retVal



class Switch(object):
	switchIndex=0
	pieceIndex=0
	laneDistancesToNextSwitch=[]
	optimalLaneOrder=[]

	def __init__ (self, switchIndex):
		self.switchIndex = switchIndex

	def setPieceIndex (self, pieceIndex):
		self.pieceIndex = pieceIndex

	def addLanesToDistances(self, laneDistances):
		if len(self.laneDistancesToNextSwitch) == 0:
			self.laneDistancesToNextSwitch = laneDistances
		else:
			for index, pieceDistance in enumerate(laneDistances):
				self.laneDistancesToNextSwitch[index] += pieceDistance

	def getBestLane(self, currentLaneIndex):
		return self.laneDistancesToNextSwitch.index(min(self.laneDistancesToNextSwitch))

	def printSwitch(self):
		print 'SwitchIndex %d - PieceIndex %d' % (self.switchIndex, self.pieceIndex)
		for index, pieceDistance in enumerate(self.laneDistancesToNextSwitch):
			print 'LaneIndex %d - totalDistance %d' % (index, pieceDistance)

	def combine(self, otherSwitch):
		self.pieceIndex = otherSwitch.pieceIndex
		for index, pieceDistance in enumerate(otherSwitch.laneDistancesToNextSwitch):
			self.laneDistancesToNextSwitch[index] += pieceDistance



class Switches(object):
	allSwitches=[]

	def addSwitch(self, switchIndex, switch):
		self.allSwitches.insert(switchIndex, switch)

	def printAllSwitches(self):
		for index, switch in enumerate(self.allSwitches):
			print 'Switch %d' % (index)
			switch.printSwitch()

	def combineFirstAndLast(self):
		self.allSwitches[0].combine(self.allSwitches[-1])
		self.allSwitches[-1:]



class Piece(object):
	switchIndex = 0
	pieceIndex = 0

	def __init__ (self, pieceIndex, switchIndex):
		self.switchIndex = switchIndex
		self.pieceIndex = pieceIndex

	def getPieceLength(self, distFromCenter=0):
		return 0;

class TurnPiece (Piece):
	radius=0
	angle=0
	def __init__ (self, radius, angle, pieceIndex, switchIndex):
		super(TurnPiece, self).__init__(pieceIndex, switchIndex)
		self.radius = radius
		self.angle = angle

	def getPieceLength(self, distFromCenter=0):
		determinedDist = distFromCenter if self.angle > 0 else -distFromCenter
		length = (self.radius + determinedDist) * math.fabs(self.angle)  * D2R

		return length


class StraightPiece (Piece):
	length=0
	def __init__ (self, length, pieceIndex, switchIndex):
		super(StraightPiece, self).__init__(pieceIndex, switchIndex)
		self.length = length

	def getPieceLength(self, distFromCenter=0):
		return self.length


class PatPieces(object):
	allPieces = []
	allLanes  = Lanes()
	mySwitches = Switches()

	def __init__ (self, data):
		pieces = data["race"]["track"]["pieces"]
		lanesData = data["race"]["track"]["lanes"]
		for i, aLane in enumerate ( lanesData ):
			self.allLanes.addLane(i, aLane["distanceFromCenter"])

		switchIndex = 0
		switch = Switch(switchIndex)
		for i, aPiece in enumerate ( pieces ):
			if "angle" in aPiece:
				nextPiece = self.addTurnPiece( aPiece["radius"], aPiece["angle"],  i, switchIndex)
			else:
				nextPiece = self.addStraightPiece(aPiece["length"], i, switchIndex)

			# TODO : Turns with switches, how to do the correct calculate
			switch.addLanesToDistances(self.allLanes.calculateAllDistances(nextPiece))
			if ("switch" in aPiece):
				switch.pieceIndex = i
				self.mySwitches.addSwitch(switchIndex, switch)
				switchIndex+=1
				switch = Switch(switchIndex)


		# The last switch index and the first switch index are the same, combine
		self.mySwitches.combineFirstAndLast()
		self.mySwitches.printAllSwitches()


	def addStraightPiece(self, length, pieceIndex, switchIndex):
		piece = StraightPiece(length, pieceIndex, switchIndex)
		self.allPieces.insert(pieceIndex, piece)
		return piece

	def addTurnPiece(self, radius, angle, pieceIndex, switchIndex):
		piece = TurnPiece(radius, angle, pieceIndex, switchIndex)
		self.allPieces.insert(pieceIndex, piece)
		return piece

	def getBestLane(self, pieceIndex, laneIndex):
		switchIndex = self.allPieces[pieceIndex].switchIndex
		if (switchIndex >= len(self.mySwitches.allSwitches)):
			switchIndex = 0
		return self.mySwitches.allSwitches[switchIndex].getBestLane(laneIndex)

	def printAllPieces(self):
		for index, piece in enumerate(self.allPieces):
			length = piece.getPieceLength()			 
			print 'Piece %d of length %f' % (index, length)





class Pieces:
	def __init__ (self, data):
		pieces = data["race"]["track"]["pieces"]
		self.turnToType = {}
		self.turnToLength = {}
		self.turnToRadius = {}
		for i, aPiece in enumerate ( pieces ):
			if "angle" in aPiece and "switch" in aPiece:
				self.turnToType[i] = PIECE_TURN | PIECE_SWITCH
			elif "angle" in aPiece:
				self.turnToType[i] = PIECE_TURN
			elif "switch" in aPiece:
				self.turnToType[i] = PIECE_SWITCH
				print 'Piece %d is a switch' % ( i )
			else:
				self.turnToType[i] = PIECE_STRAIGHT
			if "length" in aPiece:
				self.turnToLength[i] = aPiece["length"]
				print 'Piece %d len %d' % ( i, self.turnToLength[i] )
			elif "angle" in aPiece:
				self.turnToLength[i] = abs(aPiece["angle"]) * D2R * aPiece["radius"]
				self.turnToRadius[i] = aPiece["radius"]
				print 'Piece %d len %d radius %.2f' % ( i, self.turnToLength[i], self.turnToRadius[i] )
		self.numTurns = len ( self.turnToType.keys() )

	def getPieceLength ( self, turnNumber ):
		return self.turnToLength[turnNumber]

	def getPieceType ( self, turnNumber ):
		return self.turnToType[turnNumber]

	def getNextPiece ( self, turn ):
		return ( turn + 1 ) % self.numTurns

	def getNextPieceType ( self, turn ):
		return self.turnToType[( turn + 1 ) % self.numTurns]

	def getRadius ( self, turn ):
		if ( PIECE_TURN & self.turnToType[turn] ):
			return self.turnToRadius[turn]
		else:
			return 1e20

	def getNextTurn ( self, turn ):
		# return the next piece number that is a PIECE_TURN
		for i in range(turn+1, turn+self.numTurns):
			nextPiece = i % self.numTurns
			nextType = self.getPieceType(nextPiece)
			if ( PIECE_TURN & nextType ):
				return nextPiece
		raise Exception("getNextTurn():You screw up the code, moron.")
	
	def getDistanceToPiece ( self, srcPiece, srcDistance, destPiece ):
		# returns distance to destPiece from srcPiece at srcDistance
		# x----x----------x-------x---x   travel -->
		#	  ^   ^			  ^
		#	  |   |			  + dstPiece
		#	  |   + srcDistance
		#	  + srcPiece
		dist = self.turnToLength[srcPiece] - srcDistance
		if destPiece < srcPiece:
			destPiece += self.numTurns
		for i in range(srcPiece+1, destPiece):
			j = i % self.numTurns
			dist += self.turnToLength[j]
		return dist

	def estimateTicksToTurn ( self,  srcPiece, srcDistance, currentSpeed, entrySpeed, decel ):
		# returns estimated number of ticks to next turn, assuming
		# throttle is set to zero now and entry speed is provided
		nextTurn = self.getNextTurn ( srcPiece )
		distToTurn = self.getDistanceToPiece ( srcPiece, srcDistance, nextTurn )
		ticks = (entrySpeed - currentSpeed ) / decel
		return ticks


			

class Logger:
	def __init__ (self, path):
		self.fp = open(path, 'wb')
	def client ( self, msg ):
		self._log ("client", msg)
	def server ( self, msg ):
		self._log ("server", msg)
	def _log(self, who, msg):
		out = "%s,%s,%s" % ( time.time(), who, msg )
		self.fp.write(out)
		self.fp.flush()
	def close ( self ):
		self.fp.close()

class NoobBot(object):

	def __init__(self, socket, name, key):
		self.log = Logger ("./data.txt")
		self.socket = socket
		self.name = name
		self.key = key
		self.trackPieces = None
		self.turnVelocity = 0.5
		self.lastDistance = 0
		self.lastAngle = 0
		self.lastTick = 0
		self.lastSlipSign = 1
		self.lastPieceIndex = 0
		self.futureLane = 1
		self.mode = 'a'
		self.tick = 0
		self.muSum = 0.0
		self.muCnt = 0
		self.muAvg = 0.0
		self.lapTimes = {}
		self.startedSwitch = False
		self.lastSpeed = 0.0

	def msg(self, msg_type, data):
		self.send(json.dumps({"msgType": msg_type, "data": data}))

	def send(self, msg):
		self.socket.send(msg + "\n")
		self.log.client ( msg + "\n" )

	def join(self):
		return self.msg("join", {"name": self.name,
								 "key": self.key})

	def throttle(self, throttle):
		self.msg("throttle", throttle)

	def switchLane(self, laneDirection):
		self.msg("switchLane", laneDirection)

	def ping(self):
		self.msg("ping", {})

	def join_race (self, track):
		print 'Joining track %s' % ( track )
		self.msg ( "joinRace", {
				"botId" : {
					"name": "BananaStand",
					"key" : self.key
				},
				"trackName" : track
			})

	def run(self, track="keimola"):
		#self.join()
		self.join_race(track)
		self.msg_loop()

	def on_game_init ( self, data ):
		self.trackPieces = Pieces ( data )
		self.patPieces = PatPieces ( data )
		for i in range( self.trackPieces.numTurns ):
			print 'getNextTurn %d --> %d' % ( i, self.trackPieces.getNextTurn(i) )

	def on_join(self, data):
		print("Joined")
		self.ping()

	def on_join_race ( self, data ):
		self.ping()

	def on_game_start(self, data):
		print("Race started")
		self.ping()

	def on_car_positions(self, data):
		for q in data:
			if not q["id"]["name"] == "BananaStand":
				continue
			us = q
			break


		lap = us["piecePosition"]["lap"]
		turn = us["piecePosition"]["pieceIndex"]
		currDist = us["piecePosition"]["inPieceDistance"]
		angle = us["angle"]
		pieceLength = self.trackPieces.getPieceLength(turn)
		turnType = self.trackPieces.getPieceType(turn) 
		nextTurnType = self.trackPieces.getNextPieceType(turn)
		radius = self.trackPieces.getRadius (turn)
		mu = None
		if ( currDist < self.lastDistance ):
			speed = self.lastSpeed
		else:
			speed = currDist - self.lastDistance
			if ( radius < 1e10 and ( turn & PIECE_TURN ) ):
				mu = ( speed * speed ) / radius
				self.muSum += mu
				self.muCnt += 1
				self.muAvg = self.muSum / self.muCnt
				
			
		# TODO: provide methods to return distance to next turn and use acc for throttle cut because we are not stopping in time to make the turn
		# TODO: Need to include time as part of derivative calculation. Races will not wait for ping backs
		self.lastDistance = currDist
		slipRate = angle - self.lastAngle
		slipSign = [-1,1][slipRate>0]
		absSlipRate = abs(slipRate)
		self.lastAngle = angle
		self.zeroCrossing = not ( slipSign == self.lastSlipSign )
		absAngle = abs(angle)
		self.lastSlipSign = slipSign
		acc = speed - self.lastSpeed

		#
		#self.throttle(0.5) # --> mu = 0.25
		# muMax muAvg(obs)
		#  0.37   0.3682 11033
		#  0.45   0.4135 CRASH 11017
		muMax = 0.41
		wantSpeed =  ( radius * muMax ) ** 0.5
		if ( wantSpeed > 100 ): wantSpeed = 100
		percComplete = currDist / pieceLength

		# DEBUG
		if turn in [1,2,3,4] or 0 == self.turnVelocity or self.mode == 'b'or absAngle > 30.0 or ( 0 == (self.tick + 1) % 13 ):
			fmt = 'Tick %05d Piece %2d/%d PC %.2f Ang %6.2f Throttle %.2f SR %8.3f Spd %6.2f Want %6.2f Acc %7.4f mu %s/%f\r' % ( self.tick, turn, turnType, percComplete, angle, self.turnVelocity, slipRate, speed, wantSpeed, acc, mu, self.muAvg )
			sys.stdout.write(fmt+"\n")
			sys.stdout.flush()
		
		# TODO: Need to figure out lane switching for more than 2 lanes
		pieceIndex = us["piecePosition"]["pieceIndex"]
		startLaneIndex = us["piecePosition"]["lane"]["startLaneIndex"]
		endLaneIndex = us["piecePosition"]["lane"]["endLaneIndex"]

		if (self.lastPieceIndex != pieceIndex):
			self.lastPieceIndex = pieceIndex
			# TODO what if it is changing
			if (startLaneIndex == endLaneIndex):
				bestLane = self.patPieces.getBestLane(pieceIndex, endLaneIndex)
				if (bestLane > endLaneIndex):
					# TODO determine left and right 
					self.switchLane("Right")
				elif (bestLane < endLaneIndex):
					self.switchLane("Left")

		if ( False ):
			if ( 1 == lap and 3 == turn and not self.startedSwitch):
				print 'SWITCH! 3'
				print '-------------', self.startedSwitch
				self.msg ("switchLane", "right")
				self.startedSwitch = True
				return
			if ( turn > 3 and turn < 13 ):
				self.startedSwitch = False
			if ( 13 == turn and not self.startedSwitch ):
				print 'SWITCH! 13'
				self.msg ("switchLane", "left")
				self.startedSwitch = True
				return
			if ( turn > 13 and turn < 25 ):
				self.startedSwitch = False
			if ( 25 == turn and not self.startedSwitch):
				print 'SWITCH! 25'
				self.msg ("switchLane", "right")
				self.startedSwitch = True
				return
			if ( turn > 25 ):
				self.startedSwitch = False

		# TODO: cross over at pieces 13 and 25?
		# TODO: Should really be distance based on breaking performance (might be N pieces ahead)
		if ( nextTurnType & PIECE_TURN ):
			#if ( percComplete >= 0.4 and speed > wantSpeed):
			nextTurnRadius = self.trackPieces.getRadius (self.trackPieces.getNextPiece(turn))
			nextTurnSpeed = ( nextTurnRadius * muMax ) ** 0.5
			if ( speed > nextTurnSpeed ) or (absSlipRate>3.0) or ( absAngle >15 and absSlipRate > 2.0  ):
				self.turnVelocity = 0.0
				self.throttle(self.turnVelocity)
				return

		if PIECE_TURN & turnType:
			#print 'want %f now %f' % ( wantSpeed, speed )
			if ( wantSpeed < speed or absAngle > 40.0):
				self.turnVelocity = 0.0
			elif ( wantSpeed > speed ):
				if absSlipRate < 1.4:
					self.turnVelocity = 1.0
				#else:
				#	self.turnVelocity = 0.5
				if ( self.turnVelocity < 0.5 ):
					self.turnVelocity = 0.5
				else:
					self.turnVelocity *= 1.1
					if self.turnVelocity > 1.0:
						self.turnVelocity = 1.0
		else:
			self.turnVelocity = 1.0

		self.throttle ( self.turnVelocity )
		self.lastSpeed = speed
		return

		# in a turn
		if PIECE_TURN & turnType:
			if absSlipRate < 1.4 and (not self.mode == 'b') and self.zeroCrossing:
				self.mode='a'
				#self.turnVelocity = 1.0			
				t = self.turnVelocity * 1.1
				if ( t > 1.0 ):
					t = 1.0
				self.turnVelocity = t
			elif absSlipRate > 3.0 or absAngle > 10.0:
				self.turnVelocity = 0.0
				self.mode = 'b'
			else:
				self.mode = 'a'
				if self.turnVelocity < 0.5:
					self.turnVelocity = 0.5
				else:
					t = self.turnVelocity * 1.1
					if ( t > 1):
						t = 1
					self.turnVelocity = t
			self.throttle(self.turnVelocity)

		# not a turn
		elif PIECE_STRAIGHT & turnType or PIECE_SWITCH & turnType:
			self.mode = 'a'
			if ( nextTurnType & PIECE_TURN ):
				percComplete = currDist / pieceLength
				if ( percComplete >= 0.5 ):
					self.turnVelocity = 0.0
					self.mode = 'b'	
				else:
					self.turnVelocity = 1.0
			else:
				self.turnVelocity = 1.0
			self.throttle ( self.turnVelocity )

	def on_crash(self, data):
		print("\n%s crashed" % data["name"])
		self.ping()

	def on_game_end(self, data):
		print("\nRace ended")
		self.ping()

	def on_error(self, data):
		print("\nError: {0}".format(data))
		self.ping()
	
	def on_spawn (self, data):
		print "\n%s spawned" % ( data["name"] )
		self.turnVelocity = 1.0
		self.mode = 'a'
		self.throttle(self.turnVelocity)
	
	def on_lap ( self, data ):
		lap, name, lapTime = data["lapTime"]["lap"], data["car"]["name"], data["lapTime"]["millis"]
		if ( not self.lapTimes.has_key(name) ):
			self.lapTimes[name] = []
		self.lapTimes[name].append(lapTime)
		print "\nLap %d Name %s Time %d" % ( lap, name, lapTime )
		self.ping()

	def msg_loop(self):
		msg_map = {
			'gameInit' : self.on_game_init,
			'join': self.on_join,
			'joinRace' : self.on_join_race,
			'gameStart': self.on_game_start,
			'carPositions': self.on_car_positions,
			'crash': self.on_crash,
			'gameEnd': self.on_game_end,
			'error': self.on_error,
			"spawn": self.on_spawn,
			'lapFinished' : self.on_lap
		}
		socket_file = s.makefile()
		line = socket_file.readline()
		while line:
			msg = json.loads(line)
			msg_type, data = msg['msgType'], msg['data']
			if msg.has_key("gameTick"):
				self.lastTick = self.tick
				self.tick = msg["gameTick"]
			self.log.server ( line )
			if msg_type in msg_map:
				msg_map[msg_type](data)
			else:
				print("\nGot {0}".format(msg_type))
				self.ping()
			line = socket_file.readline()
		self.log.close()

		# results
		print '%-25s %s' % ( "Car Name", "Lap Times" )
		for car in self.lapTimes.keys():
			print '%-25s %s' % ( car, " ".join(map(str,self.lapTimes[car])) )


if __name__ == "__main__":
	if len(sys.argv) != 5:
		print("Usage: ./run host port botname botkey")
	else:
		host, port, name, key = sys.argv[1:5]
		print("Connecting with parameters:")
		print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.connect((host, int(port)))
		bot = NoobBot(s, name, key)
		try:
				bot.run("germany")
		except:
			print 'EXCEPTION:', sys.exc_info()[:2]
			raise
