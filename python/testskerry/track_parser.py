from collections import defaultdict
from math import pi,fabs,fmod

def compute_turn_length(  radius, angle ):
	return (2 * pi * radius) * (angle/360.0)


class track_parser:
	def __init__( self, data, muMax=0.45 ):
		''' 
		INPUT data -> a dictionary from the server from raceInit

		The init routine will set up a lot of cached info about the track and expose it with helper routines.

		self.piece_len[ piecenum ] -> length of a specific piece

		self.len_at_start[lane][ piecenum ] -> cumulative track length at start of that piece.  
			Ex: piece1 is 100 units long straightaway, self.len_at_start[0][1] = 100.0
			this accounts for radius of turns and lanes as well.  I use this to tell where I am on the track
			and to compute overall length

		self.radius_at[lane][piecenum] -> again, a helper dict to account for lanes in turn radii.  
			-> piecenum must be a turn piece or you get a nokey exception

		self.switches[ piecenum ]  = <True/False> 
			-> note NO LANE NUMBER

		self.turns[ piecenum ] = <True/False>

		self.num_pieces = # (total number of pieces)

		self.last_piece = # (integer of last piece number)

		self.total_lane_length[lane] = float (total length of each lane)
			-> used to find shortest lane and to do some modulus math

		'''
		try:
			self.cars = data['race']['cars']
			self.track= data['race']['track']
		except: 
			self.errMsg('could not find key for track or cars')

		self.muMax= 0.45

		self.parse_lanes( self.track )
		self.parse_track( self.track )


	def errmsg( self, S, sysexit=False ):
		import sys
		sys.stderr.write('track_parser: %s\n' %S)
		if sysexit: sys.exit()
	
	def parse_track( self, T ):
		''' called on init, parse the track'''
		# actual per-lane length (turns are obviously shorter for inside lanes)
		self.piece_len = defaultdict(lambda: dict() )
		# cumulative distance helper
		self.len_at_start = defaultdict( lambda : defaultdict(lambda: 0) )
		# hold the radius value for each lane too (for our throttle)
		self.radius_at = defaultdict( lambda: dict() )
		# record switches
		self.switches = dict()
		# and turns
		self.turns = dict()
		# num pieces
		self.num_pieces = len( T['pieces'] )
		self.last_piece = self.num_pieces - 1
		# total lane length (for whole track)
		self.total_lane_length = dict()
		for l,dist in self.lanes:
			for n,p in enumerate(T['pieces']): 
				self.turns[n] = False
				if 'switch' in p: self.switches[n] = True
				if 'length' in p: self.piece_len[l][n] = p['length']
				if 'angle' in p:
					self.turns[n] = True
					if dist <= 0 and p['angle'] < 0: self.radius_at[l][n] = p['radius'] - fabs(dist)
					elif dist >= 0 and p['angle'] >=0: self.radius_at[l][n] = p['radius'] - dist
					else: self.radius_at[l][n] = p['radius']+fabs(dist)

					self.piece_len[l][n] = compute_turn_length( self.radius_at[l][n], fabs(p['angle']))

				# don't calculate piece length for first one
				if n == 0: continue
				self.len_at_start[l][n] = self.len_at_start[l][n-1] + self.piece_len[l][n]

		# we might need this for modulus math...
		for l,dist in self.lanes: 
			self.total_lane_length[l] = self.len_at_start[l][self.last_piece] + self.piece_len[l][self.last_piece]

		self.calc_next_turns()
		self.calc_next_switch()
		self.calc_optimal_turn_speeds()

	def calc_next_value( self, D ):
		''' Helper routine for next-value calcs.  Invoked on init by other routines.
			assume that D is a dictionary with the following format:
			D[key] = <True / False>
		    this returns the next-true value for each key'''
		N = len( D )
		firstVal = sorted( filter( lambda x: D[x], D) )[0]
		lastVal = sorted( filter( lambda x: D[x], D) )[-1]
		nextVal = dict()
		for i in range( lastVal-1, N ): nextVal[i] = firstVal
		for i in range(0,  firstVal): nextVal[i] = firstVal
		val = lastVal
		for i in range( lastVal-1, firstVal-1, -1 ):
			nextVal[i] = val
			if i in D and D[i] : val = i
		return nextVal

	def calc_next_turns( self ):
		''' Calculate the NEXT turn for a specific piece.  Create a dictionary.
			track.parser.next_turns[10] = 20	
			-> at piece 10, the next turn is piece 20
			-> if the given piece is a turn, return the NEXT turn (do not return same number)

			USE:
			self.next_turn[ piece ] = <true/false>

			You probably want to use distance_to_next_turn
			'''
		self.next_turn = self.calc_next_value( self.turns )
	
	def calc_next_switch( self ):
		'''Calculate NEXT switch opportunities.  Should be done on init.
			See calc_next_turns for more.

			USE:
			self.next_switch[ piece_number ] = next_opportunity_to_switch
			'''
		self.next_switch = self.calc_next_value( self.switches )

	def current_track_pos( self, piece, lane, in_piece ):
		'''Assume that you stay in lane.  Calculate the cumulative track distance (non-decreasing float)'''
		try: return self.len_at_start[lane][piece] + in_piece
		except: return None
	
	def distance_to_next_turn( self, piece, lane, in_piece ):
		''' given a piece number, and the distance in that piece, calculate the distance to the next turn
		    this assumes NO LANE CHANGES.  
		'''
		next_turn = self.next_turn[piece]
		if next_turn < piece: modulus = self.total_lane_length[lane]
		else: modulus = 0.0
		return fmod( modulus + self.len_at_start[lane][ next_turn ] - self.current_track_pos(piece,lane,in_piece) , 
				self.total_lane_length[lane])

	def parse_lanes( self, P ):
		try: self.lanes =  map( lambda x: (x['index'],x['distanceFromCenter']), P['lanes'] ) 
		except: self.errMsg('could not parse lanes: %s' % str( P['lanes'] ), sysexit=True )
		#print self.lanes

	def next_inside_lane( self, piece ):
		''' given a current piece, return index of the *inside* lane of the next turn '''
		# I keep lanes as tuples.  Stupid.
		L = map( lambda x: x[0], self.lanes )
		# next turn
		next_turn = self.next_turn[ piece ]
		try: return sorted( map( lambda x: ( self.radius_at[x][next_turn], x ),L )) [0][1]
		except: return None

	def is_switch_before( self, currentPiece, targetPiece ):
		''' is there an opportunity to switch between currentPiece and targetPiece?'''
		return self.next_switch[currentPiece] < targetPiece

	def calc_optimal_turn_speeds( self, mu_override=None ):
		''' assume that the class is initialized, for a given mu value, calculate the optimal speeds for turn (per-lane)

		USE:
		self.optimal_speed[ lane_number ][ piece_number ] = speed
		
		Note: piece_number must be a turn, else you'll raise an exception'''
		if mu_override == None: mu = self.muMax
		else: mu = self.muMax
		self.optimal_speed = dict()
		for l, dist in self.lanes:
			self.optimal_speed[l] = dict()
			for turn in self.radius_at[l]:
				self.optimal_speed[l][turn] = ( self.radius_at[l][turn] * mu ) ** 0.5
		return self.optimal_speed

	def distance_between( self, lane, piece1, in_piece1, piece2, in_piece2 ):
		'''used to calculate distance between cars in a lane'''
		return (in_piece2 - in_piece1) + (self.len_at_start[lane][piece2] - self.len_at_start[lane][piece1] )

if __name__ == "__main__":
	import json
	# cached gameInit data 
	DATA = json.loads( open('track.data','r').read() )
	# init the class
	TP = track_parser( DATA )
	# current "track position" as a cumulative distance (see above)
	print "Current track dist", TP.current_track_pos( 10, 1, 2.3 )
	print
	# distance to next turn from piece 36, lane 1, piece position 2.3
	print "Distance to next turn", TP.distance_to_next_turn( 36, 1, 2.3 )
	print
	# next turn inside lane
	print "Inside lane", TP.next_inside_lane( 10 )
	print
	
	# calculate the optimal turn speeds 
	print "Optimal turn speeds", TP.calc_optimal_turn_speeds()
	print
	
	# calculate the optimal turn speeds for a given mu value
	print "Optimal turn speeds for mu=0.39", TP.calc_optimal_turn_speeds( mu_override = 0.39)
	print

	# I'm at 10, is there a switch before 15?	
	print "Can I swtich before tile 15 if I am at tile 10: ", TP.is_switch_before( 10, 15 )
	print

	print TP.distance_between( 0, 10, 3.4, 22, 10.1)
